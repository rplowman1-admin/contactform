<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '24f22d049647144998850c244a7b43607f7848c0',
        'name' => '__root__',
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '24f22d049647144998850c244a7b43607f7848c0',
            'dev_requirement' => false,
        ),
        'psr/log' => array(
            'pretty_version' => '1.1.4',
            'version' => '1.1.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/log',
            'aliases' => array(),
            'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
            'dev_requirement' => false,
        ),
        'symfony/debug' => array(
            'pretty_version' => 'v2.8.52',
            'version' => '2.8.52.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/debug',
            'aliases' => array(),
            'reference' => '74251c8d50dd3be7c4ce0c7b862497cdc641a5d0',
            'dev_requirement' => false,
        ),
        'symfony/event-dispatcher' => array(
            'pretty_version' => 'v2.8.52',
            'version' => '2.8.52.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/event-dispatcher',
            'aliases' => array(),
            'reference' => 'a77e974a5fecb4398833b0709210e3d5e334ffb0',
            'dev_requirement' => false,
        ),
        'symfony/http-foundation' => array(
            'pretty_version' => 'v2.8.52',
            'version' => '2.8.52.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/http-foundation',
            'aliases' => array(),
            'reference' => '3929d9fe8148d17819ad0178c748b8d339420709',
            'dev_requirement' => false,
        ),
        'symfony/http-kernel' => array(
            'pretty_version' => 'v2.8.52',
            'version' => '2.8.52.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/http-kernel',
            'aliases' => array(),
            'reference' => 'c3be27b8627cd5ee8dfa8d1b923982f618ec521c',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-ctype' => array(
            'pretty_version' => 'v1.24.0',
            'version' => '1.24.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-ctype',
            'aliases' => array(),
            'reference' => '30885182c981ab175d4d034db0f6f469898070ab',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-mbstring' => array(
            'pretty_version' => 'v1.24.0',
            'version' => '1.24.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-mbstring',
            'aliases' => array(),
            'reference' => '0abb51d2f102e00a4eefcf46ba7fec406d245825',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php54' => array(
            'pretty_version' => 'v1.20.0',
            'version' => '1.20.0.0',
            'type' => 'metapackage',
            'install_path' => NULL,
            'aliases' => array(),
            'reference' => '37285b1d5d13f37c8bee546d8d2ad0353460c4c7',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php55' => array(
            'pretty_version' => 'v1.20.0',
            'version' => '1.20.0.0',
            'type' => 'metapackage',
            'install_path' => NULL,
            'aliases' => array(),
            'reference' => 'c17452124a883900e1d73961f9075a638399c1a0',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php56' => array(
            'pretty_version' => 'v1.20.0',
            'version' => '1.20.0.0',
            'type' => 'metapackage',
            'install_path' => NULL,
            'aliases' => array(),
            'reference' => '54b8cd7e6c1643d78d011f3be89f3ef1f9f4c675',
            'dev_requirement' => false,
        ),
        'symfony/routing' => array(
            'pretty_version' => 'v2.8.52',
            'version' => '2.8.52.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/routing',
            'aliases' => array(),
            'reference' => '8b0df6869d1997baafff6a1541826eac5a03d067',
            'dev_requirement' => false,
        ),
        'twig/twig' => array(
            'pretty_version' => 'v3.3.8',
            'version' => '3.3.8.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../twig/twig',
            'aliases' => array(),
            'reference' => '972d8604a92b7054828b539f2febb0211dd5945c',
            'dev_requirement' => false,
        ),
    ),
);

<?php
namespace Controllers;

require_once 'Error.php';

use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use Twig\Error as TwigError;
use Controllers\Error as AppError;

//get relative path for files
$relPath = '.';
if (!file_exists($relPath . '/templates'))
    $relPath = '..';

$path = $relPath . '/templates';

//use twig templates
$twig_loader = new FilesystemLoader($path);
$twig = new Environment($twig_loader);

class View {
    //twig template wrapper instance
    private static $view;
    //variables to pass to template
    private static $viewArr = [];
    private static $errors = [];

    public static function setTemplate($template, $vars = []) {
        global $twig;
        $template = htmlspecialchars($template);

        //twig template wrapper instance
        self::$view = $twig->load($template);

        if($vars && is_array($vars)) {
            foreach ($vars as $key => $value) {
                self::setViewVar($key, $value);
            }
        }
    }

    private static function setViewVar($key, $value) {
        $key = htmlspecialchars($key);

        if($key && isset($value)) {
            self::$viewArr[$key] = $value;
        }
    }

    public static function display() {
        if(self::$errors) {
            //Add errors to template variables
            self::$viewArr['errors'] = self::$errors;
            //unset errors array
            self::$errors = [];
        }

        try {
            echo self::$view->render(self::$viewArr);
        } catch (TwigError\LoaderError $e) {
            global $relPath;

            $type = 'Twig Loader Error';
            $error = new AppError($type, $e);
            include($relPath . '/PageError.php');
            exit();
        } catch (TwigError\RuntimeError $e) {
            global $relPath;

            $type = 'Twig Runtime Error';
            $error = new AppError($type, $e);
            include($relPath . '/PageError.php');
            exit();
        } catch (TwigError\SyntaxError $e) {
            global $relPath;

            $type = 'Twig Syntax Error';
            $error = new AppError($type, $e);
            include($relPath . '/PageError.php');
            exit();
        } catch (\Exception $e) {
            global $relPath;

            $type = 'Unknown Render Error';
            $error = new AppError($type, $e);
            include($relPath . '/PageError.php');
            exit();
        }
    }

    public static function setError($type = 'Unknown Error', $exception = null) {
        $type = htmlspecialchars($type);
        $error = new AppError($type, $exception);

        self::$errors[] = $error;

//        echo self::$view->renderBlock('BlockError.twig', array('message' => $message, 'type' => $type));
    }
}
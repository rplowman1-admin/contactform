<?php

//get relative path for View class
$path = '../Controllers/View.php';
if (!file_exists($path))
    $path = './Controllers/View.php';

require_once $path;
use Controllers\View;

class Database {
    //DSN values for Mac Docksal
//    private static string $dsn = 'mysql:host=db;dbname=rpprofile';
    //DSN values for RD CWI
    private static $dsn = 'mysql:host=localhost;dbname=rpprofile';
    private static $username = 'swdv_usr';
    private static $password = 'Pa$$w0rd';
    private static $options = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION);
    private static $db;

    public static function getDB() {
        if(!isset(self::$db)) {
            try {
                self::$db = new PDO(self::$dsn, self::$username, self::$password, self::$options);
            } catch (PDOException $e) {
                View::setError('PDOException', $e);
                View::display();
                exit();
            }
        }

        return self::$db;
    }
}

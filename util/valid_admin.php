<?php
require_once('../Model/AdminDB.php');
require_once '../Controllers/View.php';
use Controllers\View;

$email = '';
$password = '';
if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
    $email = $_SERVER['PHP_AUTH_USER'];
    $password = $_SERVER['PHP_AUTH_PW'];
}

try {
    $adminDB = new AdminDB();
    if (!$adminDB->is_valid_admin_login($email, $password)) {
        header('WWW-Authenticate: Basic realm="Admin"');
        header('HTTP/1.0 401 Unauthorized');
        exit();
    }
} catch(Exception|\Error $e) {
    //TODO: handle exceptions thrown from AdminDB
    exit();
}
?>

<?php

namespace Controllers;

class Error {
    private $type;
    private $message = 'Unknown Error';
    private $file = 'Unknown File';
    private $line = 'Unknown';
    //Throwable instance
    private $e;

    public function __construct($type = 'Undefined Type', $eObj = null) {
        $type = htmlspecialchars($type);
        $this->type = $type;

        if($eObj instanceof \Exception) {
            $this->e = $eObj;
            $this->message = $eObj->getMessage();
            $this->file = $eObj->getFile();
            $this->line = $eObj->getLine();

            if(!$type)
                $this->type = $eObj->getCode();
        }
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message)
    {
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $file
     */
    public function setFile(string $file)
    {
        $this->file = $file;
    }

    /**
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param string $line
     */
    public function setLine(string $line)
    {
        $this->line = $line;
    }

    /**
     * @return string
     */
    public function getLine()
    {
        return $this->line;
    }
}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Contact Form</title>
    <link rel="stylesheet" href="../style/imports.css">
    <link rel="stylesheet" href="./style/imports.css">
</head>
<body>
<div class="page PageError">
    <div class="block BlockError">
        <h2>Error Type: <?php echo $error->getType() ?></h2>
        <p>Found at: <?php echo $error->getFile() ?> on line <?php echo $error->getLine() ?></p>
        <br>
        <p>Error: <?php echo $error->getMessage() ?></p>
    </div>
</div>
</body>
</html>
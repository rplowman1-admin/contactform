<?php
require_once('EmployeesDB.php');

class MessagesDB {

    public static function insertMessage($nameFirst, $nameLast, $email, $phone, $message) {
        $db = Database::getDB();

        //sanitize vars
        $nameFirst = htmlspecialchars($nameFirst);
        $nameLast = htmlspecialchars($nameLast);
        $email = filter_var($email, FILTER_SANITIZE_EMAIL);
        $message = htmlspecialchars($message);

        $query = 'INSERT into messages 
                    (first_name, last_name, email_address, phone_number, message, assigned_to, created_at)
                    VALUES
                    (:nameFirst, :nameLast, :email, :phone, :message, :employeeId, CURRENT_TIMESTAMP)';

        $statement = $db->prepare($query);

        //bind values
        $statement->bindValue(':nameFirst', $nameFirst);
        $statement->bindValue(':nameLast', $nameLast);
        $statement->bindValue(':email', $email);
        $statement->bindValue(':message', $message);

        //check for optional phone var
        if(!!$phone) {
            $phone = htmlspecialchars($phone);

            //add phone var to statement
            $statement->bindValue(':phone', $phone);
        } else {
            //add null phone to statement
            $statement->bindValue(':phone', null);
        }

        //get employeeID
        $employeeId = EmployeesDB::getEmployeeId();

        //add employee to statement
        $statement->bindValue(':employeeId', $employeeId);

        //insert row
        $statement->execute();
        $statement->closeCursor();
    }

    public static function getMessagesByEmployee($employeeId): array {
        $db = Database::getDB();

        //sanitize var
        $employeeId = filter_var($employeeId, FILTER_SANITIZE_NUMBER_INT);

        //get list of messages for employee, in order of most recently created
        $query = 'SELECT 
                `first_name`, `last_name`, `email_address`, `phone_number`, `message`, `assigned_to`, `id`, `created_at` 
            FROM messages WHERE `assigned_to`=:employeeId
            ORDER BY `created_at` DESC';

        $statement = $db->prepare($query);

        //bind value
        $statement->bindValue(':employeeId', $employeeId);

        $statement->execute();
        $result = $statement->fetchAll();
        $statement->closeCursor();

        return $result;
    }

    public static function updateMessage($messageId, $newContent) {
        $db = Database::getDB();

        //sanitize vars
        $messageId = filter_var($messageId, FILTER_SANITIZE_NUMBER_INT);
        $newContent = htmlspecialchars($newContent);

        //update message content
        $query = 'UPDATE messages 
                SET `message`=:newContent, `updated_at`=CURRENT_TIMESTAMP
                WHERE `id`=:messageId';

        $statement = $db->prepare($query);

        //bind values
        $statement->bindValue(':newContent', $newContent);
        $statement->bindValue(':messageId', $messageId);

        //update record
        $statement->execute();
        $statement->closeCursor();
    }

    public static function deleteMessage($messageId) {
        $db = Database::getDB();

        //sanitize var
        $messageId = filter_var($messageId, FILTER_SANITIZE_NUMBER_INT);

        //delete message
        $query = 'DELETE FROM messages 
                WHERE `id`=:messageId';

        $statement = $db->prepare($query);

        //bind value
        $statement->bindValue(':messageId', $messageId);

        //delete record
        $statement->execute();
        $statement->closeCursor();
    }
}


<?php
//for chapter 14 hw
class Employee {
    private $id;
    private $firstName;
    private $lastName;
    private $email;

    public function __construct($id, $firstName, $lastName) {
        //Sanitize params
        $id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);
        $firstName = htmlspecialchars($firstName);
        $lastName = htmlspecialchars($lastName);

        $this->id = $id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    public function getId() {
        return $this->id;
    }

    public function getFullName() {
        return $this->firstName . ' ' . $this->lastName;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $email = filter_var($email, FILTER_SANITIZE_EMAIL);

        $this->email = $email;
    }
}

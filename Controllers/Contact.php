<?php

namespace Controllers;

require_once('./Model/MessagesDB.php');
require_once('./Model/Database.php');

use Exception;
use MessagesDB;

class Contact {
    public static function handleSubmit() {
        $nameFirst = filter_input(INPUT_POST, 'nameFirstInput');
        $nameLast = filter_input(INPUT_POST, 'nameLastInput');
        $email = filter_input(INPUT_POST, 'emailInput', FILTER_SANITIZE_EMAIL);
        $phone = filter_input(INPUT_POST, 'telephoneInput');
        $message = filter_input(INPUT_POST, 'message');

        //Only submit if all required fields are set
        if(!!$nameFirst && !!$nameLast && !!$email && !!$message) {
            try {
                MessagesDB::insertMessage(htmlspecialchars($nameFirst), htmlspecialchars($nameLast), htmlspecialchars($email), htmlspecialchars($phone), htmlspecialchars($message));
            } catch(Exception|\Error $e) {
                View::setError('', $e);
                View::display();
                exit();
            }
        }
    }
}
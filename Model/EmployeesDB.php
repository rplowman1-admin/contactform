<?php
class EmployeesDB {
    public static function getEmployeeId() {
        $db = Database::getDB();

        //get a randomly selected employeeId
        $query = 'SELECT `id` FROM employees ORDER BY RAND() LIMIT 1';

        $statement = $db->prepare($query);
        $statement->execute();
        $result = $statement->fetch();
        $statement->closeCursor();

        return $result['id'];
    }

    public static function getEmployees(): array {
        $db = Database::getDB();

        //get an alphabetized list of employees
        $query = 'SELECT `id`, `first_name`, `last_name` FROM employees ORDER BY `last_name` ASC';

        $statement = $db->prepare($query);
        $statement->execute();
        $result = $statement->fetchAll();
        $statement->closeCursor();

        return $result;
    }

    public static function getEmployeesAllCols(): array {
        $db = Database::getDB();

        //get an alphabetized list of employees
        $query = 'SELECT * FROM employees ORDER BY `last_name` ASC';

        $statement = $db->prepare($query);
        $statement->execute();
        $result = $statement->fetchAll();
        $statement->closeCursor();

        return $result;
    }
}

<?php

namespace Controllers;

require_once('../Model/Database.php');
require_once('../Model/MessagesDB.php');
require_once('../Model/EmployeesDB.php');
require_once('../Controllers/Employee.php');
require_once('../Controllers/Message.php');

use Employee;
use Exception;
use Message;
use EmployeesDB;
use MessagesDB;

//used to display messages on Admin
class Admin {
    public static function listEmployees(): array {
        try {
            $employeesArr = EmployeesDB::getEmployees();
        } catch(Exception|\Error $e) {
            View::setError('', $e);
            View::display();
            exit();
        }

        return array_map(function($employee) {
            $employeeId = $employee['id'];
            $firstName = $employee['first_name'];
            $lastName = $employee['last_name'];

            return new Employee($employeeId, $firstName, $lastName);
        }, $employeesArr);
    }

    public static function getSelectedId() {
        $inputType = INPUT_POST;

        if (!$_POST && $_GET) {
            $inputType = INPUT_GET;
        }

        return filter_input($inputType, 'employeeSelect', FILTER_VALIDATE_INT);
    }

    public static function getMessagesByEmployee($selectedId): array {
        try {
            $messagesArr = MessagesDB::getMessagesByEmployee($selectedId);
        } catch(Exception|\Error $e) {
            View::setError('', $e);
            View::display();
            exit();
        }

        return array_map(function($message) {
            $newMessage = new Message(
                $message['id'],
                $message['first_name'],
                $message['last_name'],
                $message['email_address'],
                $message['phone_number'],
                $message['message'],
                $message['assigned_to']);

            $newMessage->setCreatedAt($message['created_at']);

            return $newMessage;
        }, $messagesArr);
    }

    public static function editMessage() {
        $message = filter_input(INPUT_POST, 'editContent');
        $messageId = filter_input(INPUT_POST, 'messageId', FILTER_VALIDATE_INT);

        try {
            MessagesDB::updateMessage($messageId, htmlspecialchars($message));
        } catch(Exception|\Error $e) {
            View::setError('', $e);
            View::display();
            exit();
        }
    }

    public static function deleteMessage() {
        $messageId = filter_input(INPUT_GET, 'messageId', FILTER_VALIDATE_INT);

        try {
            MessagesDB::deleteMessage($messageId);
        } catch(Exception|\Error $e) {
            View::setError('', $e);
            View::display();
            exit();
        }
    }

    public static function getAllEmployees(): array {
        try {
            $dataArr = EmployeesDB::getEmployeesAllCols();
        } catch(Exception|\Error $e) {
            View::setError('', $e);
            View::display();
            exit();
        }

        return array_map(function($row) {
            $id = $row['id'];
            $firstName = $row['first_name'];
            $lastName = $row['last_name'];
            $email = $row['email_address'];

            $employee = new Employee($id, $firstName, $lastName);
            $employee->setEmail($email);

            return $employee;
        }, $dataArr);
    }
}
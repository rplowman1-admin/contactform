<?php
require_once 'vendor/autoload.php';
require 'Controllers/Contact.php';
require_once 'Controllers/View.php';

use Controllers\Contact;
use Controllers\View;

$action = filter_input(INPUT_POST, 'action');

//set default template
View::setTemplate('PageContact.twig', []);

if ($action == 'submitNewContact') {
    Contact::handleSubmit();
//    TODO: Add success/error message
}

View::display();

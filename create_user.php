<?php
//    YOU ONLY NEED TO RUN THIS ONCE PER USER PER DATABASE

//In phpmyadmin, use rpprofile & run:
//USE rpprofile;
//CREATE TABLE administrators (
//    adminID           INT            NOT NULL   AUTO_INCREMENT,
//  emailAddress      VARCHAR(255)   NOT NULL,
//  password          VARCHAR(255)   NOT NULL,
//  firstName         VARCHAR(60),
//  lastName          VARCHAR(60),
//  PRIMARY KEY (adminID)
//);

//$dsn = 'mysql:host=db;dbname=rpprofile';
$dsn = 'mysql:host=localhost;dbname=rpprofile';
$username = 'root';
//$password = 'root';
$password = 'Pa$$w0rd';
$options = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION);
try {
    $db = new PDO($dsn, $username, $password, $options);
} catch (PDOException $e) {
    $error = $e->getMessage();
    include('PageError.php');
    exit();
}
$firstName = 'Sparkles';
$lastName = 'Unicorn';
$email = 'sparkles@cwi.edu';
$password = 'sesame';
$hash = password_hash($password, PASSWORD_DEFAULT);
$query = 'INSERT INTO administrators (emailAddress, password, firstName, lastName)
              VALUES (:email, :password, :firstName, :lastName)';
$statement = $db->prepare($query);
$statement->bindValue(':email', $email);
$statement->bindValue(':password', $hash);
$statement->bindValue(':firstName', $firstName);
$statement->bindValue(':lastName', $lastName);
$statement->execute();
$statement->closeCursor();

echo "Inserted: $email pw: $password hash: $hash<br>";
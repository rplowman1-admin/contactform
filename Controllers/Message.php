<?php
// set the default timezone to use.
date_default_timezone_set('America/Boise');

class Message {
    private $id;
    private $firstName;
    private $lastName;
    private $email;
    private $phone;
    private $content;
    private $employeeId;
    private $createdAt;

    public function __construct($id, $firstName, $lastName, $email, $phone, $content, $employeeId)
    {
        //Sanitize params
        $id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);
        $firstName = htmlspecialchars($firstName);
        $lastName = htmlspecialchars($lastName);
        $email = filter_var($email, FILTER_SANITIZE_EMAIL);
        $content = htmlspecialchars($content);
        $employeeId = filter_var($employeeId, FILTER_SANITIZE_NUMBER_INT);

        if(!!$phone) {
            $phone = htmlspecialchars($phone);
            $this->phone = $phone;
        }

        $this->id = $id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->email = $email;
        $this->content = $content;
        $this->employeeId = $employeeId;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAssignedId() {
        return $this->employeeId;
    }

    public function getFullName()
    {
        return $this->firstName . ' ' . $this->lastName;
    }

    public function getPhone() {
        if($this->phone)
            return $this->phone;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getContent() {
        return $this->content;
    }

    public function getFormattedCreatedAt() {
        return date('m.d.Y', $this->createdAt);
    }

    public function setCreatedAt($datetime) {
        $this->createdAt = $datetime;
    }
}


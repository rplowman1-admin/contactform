<?php
require_once '../vendor/autoload.php';
require_once '../Controllers/Admin.php';
require_once '../Controllers/View.php';
require '../util/secure_conn.php';
require '../util/valid_admin.php';

use Controllers\Admin;
use Controllers\View;

//get action from POST
$action = filter_input(INPUT_POST, 'action');
//check GET if no action on POST
if ($action == NULL) {
    $action = filter_input(INPUT_GET, 'action');
}

//default admin template
View::setTemplate('PageAdmin.twig');
//every template needs the list of employees
$varArray['employees'] = Admin::listEmployees();

//load templates and process submit actions
switch($action) {
    case 'list-employees':
        View::setTemplate('ListEmployees.twig', array('employees' => Admin::getAllEmployees()));
        break;
    case 'deleteMessage':
        //write to database
        Admin::deleteMessage();
        $varArray['selectedId'] = Admin::getSelectedId();
        //refresh messages list
        $varArray['messages'] = Admin::getMessagesByEmployee($varArray['selectedId']);
        View::setTemplate('PageAdmin.twig', $varArray);
        break;
    case 'editMessage':
        //write to database
        Admin::editMessage();
        $varArray['selectedId'] = Admin::getSelectedId();
        //refresh messages list
        $varArray['messages'] = Admin::getMessagesByEmployee($varArray['selectedId']);
        View::setTemplate('PageAdmin.twig', $varArray);
        break;
    case 'selectEmployee':
        $varArray['selectedId'] = Admin::getSelectedId();
        //refresh messages list
        $varArray['messages'] = Admin::getMessagesByEmployee($varArray['selectedId']);
        View::setTemplate('PageAdmin.twig', $varArray);
        break;
    default:
        View::setTemplate('PageAdmin.twig', $varArray);
}

View::display();
